alias apples="echo are the best"
alias partyparrot="curl parrot.live"
alias ds="docker stats --no-stream"
tmux() {
  echo "$1"
  if [[ "$1" == "a" ]]
  then
    /usr/bin/tmux $@ -d
  else
    /usr/bin/tmux $@
  fi
}
rm() {
  local t=$(mktemp -d);
  mv $@ $t/;
}
alias .rm="/usr/bin/rm"
alias src="source ~/.bashrc"
