echo -------- bashrc
diff ~/.bashrc bashrc
echo -------- bash_alias
diff ~/.bash_aliases bash_aliases
echo -------- vimrc
diff ~/.vimrc vimrc
echo -------- tmux.conf
diff ~/.tmux.conf tmux.conf
