PC Stats
AMD Ryzen 5 5600X 3.7 Ghz 6-Core
ASRock X570 Phantom Gaming 4 ATX AM4 Motherboard
48GB of ram (2x8GB sticks and 2x16GB sticks)

1TB nvme Crucial P1 M.2-2280 (/dev/nvme0n1p5)
1TB HDD Seagate ST1000DM003-1CH1 (/dev/sda)
1TB HDD WDC WD10EZEX-08W (/dev/sdb)
1TB HDD WDC WD10EZEX-00B (/dev/sdc)

500W PSU (be quiet!)
